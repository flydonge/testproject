const { waitForDomChange } = require('@testing-library/dom')
const request = require('supertest')
const app = require('../app')

describe('GET /'. () => {
    if('정상적인 요청, 200', (done) => {
        request(app)
            .get('/')
            .expect(200)
            .end((err, res) => {
                if(err)
                    throw err
                done()
            })
    })
})
